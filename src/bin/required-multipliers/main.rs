mod chacha;
mod keccak;

fn main() {
    for len in (0..=chacha::MAX_LEN).step_by(16) {
        let metrics = chacha::compute(len);
        println!("ChaCha20: {:?} for {} bytes", metrics, len);
    }

    for len in (0..=keccak::MAX_LEN).step_by(32) {
        let metrics = keccak::compute_sha3_256(len);
        println!("SHA3-256: {:?} for {} bytes", metrics, len);
    }
}
