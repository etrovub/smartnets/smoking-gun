use bitvec::prelude as bv;
use failure::*;
use std::io::BufRead;

use bulletproofs::r1cs::*;
use curve25519_dalek::scalar::Scalar;

#[derive(Debug)]
pub enum Gate {
    And(usize, usize, usize),
    Xor(usize, usize, usize),
    Inv(usize, usize),
    Eqw(usize, usize),
}

#[derive(Debug)]
pub struct BristolCircuit<GatesT, WiresT> {
    gates: GatesT, // [Gate]
    inputs: usize,
    outputs: usize,
    input_wires: WiresT,  // [usize]
    output_wires: WiresT, // [usize]
    wires: usize,
}

pub type DynBristolCircuit = BristolCircuit<Vec<(Gate, Option<usize>)>, Vec<usize>>;

impl BristolCircuit<Vec<(Gate, Option<usize>)>, Vec<usize>> {
    fn parse_circuit<It, E>(gates: usize, lines: It) -> Result<Vec<(Gate, Option<usize>)>, Error>
    where
        It: Iterator<Item = Result<String, E>>,
        E: Into<Error> + std::error::Error + Send + Sync + 'static,
    {
        let mut gates = Vec::with_capacity(gates);

        for line in lines {
            let line = line?; // UTF-8
            if line.is_empty() {
                continue;
            }

            let mut split: Vec<_> = line.split(" ").collect();
            let command = split.pop().ok_or(format_err!("Empty line with content."))?;

            let split: Result<Vec<usize>, _> = split.into_iter().map(str::parse).collect();
            let split = split?;

            if split.len() == 5 {
                ensure!(split[0] == 2, "Number of inputs for XOR|AND must be 2");
                ensure!(split[1] == 1, "Number of outputs for XOR|AND must be 1");

                let i1 = split[2];
                let i2 = split[3];
                let o = split[4];

                match command {
                    "XOR" => gates.push((Gate::Xor(i1, i2, o), None)),
                    "AND" => gates.push((Gate::And(i1, i2, o), None)),
                    _ => bail!("Line of len 6 cannot be `{}'", command),
                }
            } else {
                ensure!(
                    split.len() == 4,
                    "Cannot parse line `{}' with {} components.",
                    line,
                    split.len() + 1
                );

                ensure!(split[0] == 1, "Number of inputs for INV|EQW must be 1");
                ensure!(split[1] == 1, "Number of outputs for INV|EQW must be 1");

                let i = split[2];
                let o = split[3];

                match command {
                    "INV" => gates.push((Gate::Inv(i, o), None)),
                    "EQW" => gates.push((Gate::Eqw(i, o), None)),
                    _ => bail!("Line of len 5 cannot be `{}'", command),
                }
            }
        }

        let mut used_gates = std::collections::HashSet::<usize>::new();
        for (ref gate, ref mut unused) in gates.iter_mut().rev() {
            match gate {
                Gate::Xor(w1, w2, w3) | Gate::And(w1, w2, w3) => {
                    if used_gates.insert(*w1) {
                        *unused = Some(*w1);
                    }
                    if used_gates.insert(*w2) {
                        *unused = Some(*w1);
                    }
                    if used_gates.insert(*w3) {
                        *unused = Some(*w1);
                    }
                }
                Gate::Inv(w1, w2) | Gate::Eqw(w1, w2) => {
                    if used_gates.insert(*w1) {
                        *unused = Some(*w1);
                    }
                    if used_gates.insert(*w2) {
                        *unused = Some(*w1);
                    }
                }
            }
        }

        Ok(gates)
    }

    pub fn parse_old<R: BufRead>(stream: R) -> Result<Self, Error> {
        let mut lines = stream.lines();

        let l1 = lines.next().ok_or(format_err!("No first line"))??; // n_gates, n_wires
        let mut spliterator = l1.split(" ");
        let gates: usize = spliterator
            .next()
            .ok_or(format_err!("No values on first line"))?
            .parse()?;
        let wires = spliterator
            .next()
            .ok_or(format_err!("No values on first line"))?
            .parse()?;

        // IO count
        let inputs = 2;
        let outputs = 1;

        let l2 = lines.next().ok_or(format_err!("No second line"))??;
        let mut spliterator = l2.split(" ");
        let input_wires = vec![
            spliterator
                .next()
                .ok_or(format_err!("No values on second line"))?
                .parse()?,
            spliterator
                .next()
                .ok_or(format_err!("No values on second line"))?
                .parse()?,
        ];
        let output_wires = vec![spliterator
            .next()
            .ok_or(format_err!("No second value on second line"))?
            .parse()?];

        let gates = Self::parse_circuit(gates, lines)?;

        Ok(BristolCircuit {
            gates,
            inputs,
            outputs,
            input_wires,
            output_wires,
            wires,
        })
    }

    pub fn parse<R: BufRead>(stream: R) -> Result<Self, Error> {
        let mut lines = stream.lines();

        let l1 = lines.next().ok_or(format_err!("No lines"))??; // n_gates, n_wires
        let mut spliterator = l1.split(" ");
        let gates: usize = spliterator
            .next()
            .ok_or(format_err!("No values on first line"))?
            .parse()?;
        let wires = spliterator
            .next()
            .ok_or(format_err!("No values on first line"))?
            .parse()?;

        // Inputs value count, wires per input value
        let l2 = lines.next().ok_or(format_err!("No second line"))??;
        let mut spliterator = l2.split(" ");
        let inputs = spliterator
            .next()
            .ok_or(format_err!("No values on second line"))?
            .parse()?;
        let mut input_wires = Vec::with_capacity(inputs);
        for count in spliterator {
            if count.is_empty() {
                continue;
            }
            input_wires.push(count.parse()?);
        }

        // Output value count, wires per output value
        let l3 = lines.next().ok_or(format_err!("No third line"))??;
        let mut spliterator = l3.split(" ");
        let outputs = spliterator
            .next()
            .ok_or(format_err!("No values on third line"))?
            .parse()?;
        let mut output_wires = Vec::with_capacity(outputs);
        for count in spliterator {
            if count.is_empty() {
                continue;
            }
            output_wires.push(count.parse()?);
        }

        debug_assert_eq!(input_wires.len(), inputs);
        debug_assert_eq!(output_wires.len(), outputs);

        let gates = Self::parse_circuit(gates, lines)?;

        Ok(BristolCircuit {
            gates,
            inputs,
            outputs,
            input_wires,
            output_wires,
            wires,
        })
    }
}

impl<'a, GatesT, WiresT> BristolCircuit<GatesT, WiresT>
where
    GatesT: AsRef<[(Gate, Option<usize>)]>
        + std::ops::Index<usize, Output = (Gate, Option<usize>)>
        + 'static,
    WiresT: AsRef<[usize]> + std::ops::Index<usize, Output = usize> + 'static,
    &'a GatesT: IntoIterator<Item = &'a (Gate, Option<usize>)>,
{
    pub fn gadget<'b: 'a, In, CS: ConstraintSystem>(
        &'a self,
        cs: &mut CS,
        inputs: &'b [In],
        mirror_bits: bool,
    ) -> Vec<Vec<LinearCombination>>
    where
        In: std::borrow::Borrow<[LinearCombination]>,
    {
        let mut wires: Vec<LinearCombination> = Vec::with_capacity(self.wires);
        for (i, input) in inputs.into_iter().enumerate() {
            let input = input.borrow();
            debug_assert_eq!(input.len(), self.input_wires[i]);

            wires.extend(input.into_iter().cloned());
        }
        assert_eq!(wires.len() % 8, 0);
        wires.resize(self.wires, LinearCombination::default());

        let mut multiplier_count = 0;
        let mut drop_count = 0;

        for (gate_i, (gate, unused)) in (&self.gates).into_iter().enumerate() {
            use Gate::*;
            match gate {
                And(i1, i2, o) => {
                    let and = cs.multiply(wires[*i1].clone(), wires[*i2].clone());
                    multiplier_count += 1;
                    wires[*o] = and.2.into();
                }
                Xor(i1, i2, o) => {
                    let and = cs.multiply(wires[*i1].clone(), wires[*i2].clone());
                    multiplier_count += 1;
                    wires[*o] = wires[*i1].clone() + wires[*i2].clone() - and.2 * 2u32;
                }
                Inv(i, o) => {
                    wires[*o] = -wires[*i].clone() + Scalar::one();
                }
                Eqw(i, o) => {
                    wires[*o] = wires[*i].clone();
                }
            }

            if let Some(i) = unused {
                wires[*i] = LinearCombination::default();
                drop_count += 1;
            }

            if gate_i == 8000 {
                panic!("Memory going to overflow. Quitting.");
            }

            println!(
                "Gate {} processed. Allocated {} multipliers. Dropped {} gates.",
                gate_i, multiplier_count, drop_count
            );
        }

        assert_eq!(
            self.outputs, 1,
            "More than a single output not yet supported."
        );
        let out = &mut wires[(self.wires - self.output_wires[0])..];

        if mirror_bits {
            for i in 0..(out.len() / 8) {
                let out = &mut out[i * 8..];
                out.swap(0, 7);
                out.swap(1, 6);
                out.swap(2, 5);
                out.swap(3, 4);
            }
        }

        vec![out.into()]
    }

    pub fn evaluate<In>(&'a self, inputs: Vec<In>, mirror_bits: bool) -> Vec<Vec<u8>>
    where
        In: AsRef<[u8]>,
    {
        use bv::*;

        debug_assert_eq!(inputs.len(), self.inputs);

        let mut wires = BitVec::with_capacity(self.wires);
        for (i, input) in inputs.into_iter().enumerate() {
            let input = BitSlice::<Lsb0, u8>::from_slice(input.as_ref());
            debug_assert_eq!(input.len(), self.input_wires[i]);

            wires.extend(input.iter().map(|b| *b));
        }
        wires.resize(self.wires, false);

        let wires = wires.as_mut_bitslice();
        debug_assert_eq!(wires.len(), self.wires);

        for (gate, _drop_until) in &self.gates {
            use Gate::*;
            match gate {
                And(i1, i2, o) => {
                    wires.set(*o, wires[*i1] & wires[*i2]);
                }
                Xor(i1, i2, o) => {
                    wires.set(*o, wires[*i1] ^ wires[*i2]);
                }
                Inv(i, o) => {
                    wires.set(*o, !wires[*i]);
                }
                Eqw(i, o) => {
                    wires.set(*o, wires[*i]);
                }
            }
        }

        assert_eq!(
            self.outputs, 1,
            "More than a single output not yet supported."
        );
        let out: &mut BitSlice<Lsb0, u8> = &mut wires[(self.wires - self.output_wires[0])..];
        assert!(
            out.len() % 8 == 0,
            "Cannot support outputs of non 8-multiple lengths."
        );
        assert_eq!(out.len(), self.output_wires[0]);

        if mirror_bits {
            for i in 0..(out.len() / 8) {
                let out: &mut BitSlice<_, u8> = &mut out[i * 8..];
                out.swap(0, 7);
                out.swap(1, 6);
                out.swap(2, 5);
                out.swap(3, 4);
            }
        }

        let out = BitVec::<_, u8>::from_bitslice(out);

        vec![out.into_vec()]
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::BufReader;

    use failure::Error;

    use super::*;

    #[derive(Clone, Copy)]
    enum Version {
        New,
        Old,
    }

    fn open_circuit(path: &str, version: Version) -> Result<DynBristolCircuit, Error> {
        let f = File::open(path)?;
        let r = BufReader::new(f);

        match version {
            Version::New => BristolCircuit::parse(r),
            Version::Old => BristolCircuit::parse_old(r),
        }
    }

    #[test]
    fn parse_all() -> Result<(), Error> {
        let circuits = &[
            // New style
            ("data/adder64.txt", Version::New, vec![64, 64], vec![64]),
            ("data/aes_128.txt", Version::New, vec![128, 128], vec![128]),
            ("data/aes_192.txt", Version::New, vec![192, 128], vec![128]),
            ("data/aes_256.txt", Version::New, vec![256, 128], vec![128]),
            ("data/divide64.txt", Version::New, vec![64, 64], vec![64]),
            ("data/keccak_f.txt", Version::New, vec![1600], vec![1600]),
            (
                "data/mult2_64.txt",
                Version::New,
                vec![64, 64],
                vec![64, 64],
            ),
            ("data/mult64.txt", Version::New, vec![64, 64], vec![64]),
            ("data/neg64.txt", Version::New, vec![64], vec![64]),
            ("data/sub64.txt", Version::New, vec![64, 64], vec![64]),
            ("data/zero_equal.txt", Version::New, vec![64], vec![1]),
            // Old style
            (
                "data/sha256Final.txt",
                Version::Old,
                vec![512, 0],
                vec![256],
            ),
        ];

        for (circuit, v, inputs, outputs) in circuits {
            println!("Reading {}", circuit);
            let circuit = open_circuit(circuit, *v)?;

            // Circuit size tests
            assert_eq!(circuit.input_wires, *inputs, "Input sizes mismatch");
            assert_eq!(circuit.output_wires, *outputs, "Output sizes mismatch");

            assert_eq!(circuit.inputs, inputs.len());
            assert_eq!(circuit.outputs, outputs.len());

            assert_eq!(
                circuit.wires,
                circuit.gates.len() + circuit.input_wires.iter().sum::<usize>()
            );
        }

        Ok(())
    }

    #[test]
    #[ignore]
    fn neg64() -> Result<(), Error> {
        let circuit = open_circuit("data/neg64.txt", Version::New)?;

        let test_vectors = vec![
            // Little Endian byte streams.
            ("0000000000000000", "0000000000000000"), // 0 -> 0
            ("0100000000000000", "ffffffffffffffff"), // 1 -> 0
            ("0000000000000080", "0000000000000080"), // -128 -> -0
        ];

        for (i, o) in test_vectors {
            let i = hex::decode(i)?;
            let o = hex::decode(o)?;

            let eval = circuit.evaluate(vec![i.clone()], true);
            assert_eq!(
                o,
                eval[0],
                "{} -> {} (expected) == {} (actual)",
                hex::encode(&i),
                hex::encode(&o),
                hex::encode(&eval[0])
            );
        }

        Ok(())
    }

    #[test]
    #[ignore]
    fn sub64() -> Result<(), Error> {
        let circuit = open_circuit("data/sub64.txt", Version::New)?;

        let test_vectors = vec![
            ("0000000000000000", "0000000000000000", "0000000000000000"),
            ("0000000000000001", "0000000000000000", "0000000000000001"),
            ("0200000000000000", "0100000000000000", "0100000000000000"),
            ("0000000000000003", "0000000000000002", "0000000000000001"),
            ("0000000000000002", "0000000000000001", "0000000000000001"),
        ];

        for (i1, i2, o) in test_vectors {
            let i1 = hex::decode(i1)?;
            let i2 = hex::decode(i2)?;
            let o = hex::decode(o)?;
            assert_eq!(vec![o], circuit.evaluate(vec![i1, i2], true));
        }

        Ok(())
    }

    #[test]
    #[ignore]
    fn keccak() -> Result<(), Error> {
        let circuit = open_circuit("data/keccak_f.txt", Version::New)?;

        // Tiny keccak order test-vectors
        // the circuit takes its 64 bit lanes in opposite order
        let test_vectors = vec![
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            "1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            "1000000000000000000000000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
        ];

        for input_str in test_vectors {
            let mut input = hex::decode(input_str)?;
            assert_eq!(input.len(), 200);

            let mut tiny = [0u64; 25];
            for i in 0..25 {
                for j in 0..8 {
                    tiny[i] |= (input[i * 8 + (7 - j)] as u64) << (8 * j);
                }
            }
            println!("    tci: {:?}", &tiny);
            tiny_keccak::keccakf(&mut tiny);
            let tiny: Vec<u8> = tiny
                .iter()
                .rev()
                .flat_map(|i| i.to_be_bytes().to_vec())
                .collect();

            // Correct lane entries
            for lane in 0..(25 / 2) {
                // Swap lane `i'
                // i with 24-i
                let i = 8 * lane;
                let i_target = (24 - lane) * 8;
                for j in 0..8 {
                    // Swap byte j
                    input.swap(i + j, i_target + j);
                }
            }
            println!("    inp: {}", hex::encode(&input));

            let output = circuit.evaluate(vec![input], true);
            let output = hex::encode(&output[0]);
            let tc = hex::encode(tiny);

            println!("    t-c: {}", &tc[0..16]); // print eight first bytes
            println!("    out: {}", &output[0..16]);
            assert_eq!(tc, output, "Unequal KAT");
        }

        Ok(())
    }

    #[test]
    #[ignore]
    fn sha2_compress_bristol() -> Result<(), Error> {
        let circuit = open_circuit("data/sha256Final.txt", Version::Old)?;

        let test_vectors = vec![
            ("00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", "da5698be17b9b46962335799779fbeca8ce5d491c0d26243bafef9ea1837a9d8"),
        ];

        for (input_str, output_str) in test_vectors {
            let input = hex::decode(input_str)?;
            assert_eq!(input.len(), 64);

            // Correct lane entries, permutation input
            println!("    inp: {}", hex::encode(&input));

            // Proof
            let output = circuit.evaluate(vec![input], false);
            let output = hex::encode(&output[0]);

            println!("    t-c: {}", &output_str[0..16]); // print eight first bytes
            println!("    out: {}", &output[0..16]);
            assert_eq!(output_str, output, "Unequal KAT");
        }

        Ok(())
    }
}
