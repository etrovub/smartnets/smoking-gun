use bitvec::prelude::*;
use bulletproofs::r1cs::{ConstraintSystem, LinearCombination};
use curve25519_dalek::scalar::Scalar;

pub struct KeccakState<const W: usize>([[[LinearCombination; W]; 5]; 5], usize);

// TODO: Normally, we would start the state with [MaybeUninit<LC>; W], but the compiler behaves
// strangely when we introduce a const generic. It seems not to know that those arrays are of equal
// size. This gets probably fixed when const generics mature a bit.
fn zero_lane<const W: usize>() -> [LinearCombination; W] {
    #[allow(deprecated)]
    let mut state: [LinearCombination; W] = unsafe { std::mem::uninitialized() };
    for k in state.iter_mut() {
        unsafe {
            ::std::ptr::write(k, LinearCombination::default());
        }
    }
    state
}

fn rot<const W: usize>(inp: &[LinearCombination; W], cnt: usize) -> [LinearCombination; W] {
    let mut out = zero_lane();

    for i in 0..W {
        out[(i + cnt) % W] = inp[i].clone();
    }

    out
}

fn not<const W: usize>(l: &[LinearCombination; W]) -> [LinearCombination; W] {
    let mut out = zero_lane();
    for i in 0..W {
        out[i] = -l[i].clone() + 1u32;
    }
    out
}

fn xor_ct<const W: usize>(lc: &[LinearCombination; W], ct: u64) -> [LinearCombination; W] {
    let mut out = zero_lane();
    for i in 0..W {
        let bit = ((ct >> i) & 1) == 1;
        if bit {
            out[i] = -lc[i].clone() + 1u32;
        } else {
            out[i] = lc[i].clone();
        }
    }
    out
}

fn xor_one<CS: ConstraintSystem>(
    cs: &mut CS,
    l: LinearCombination,
    r: LinearCombination,
) -> LinearCombination {
    let (l, r, m) = cs.multiply(l, r);
    l + r - m * 2u32
}

fn xor<CS: ConstraintSystem, const W: usize>(
    cs: &mut CS,
    l: &[LinearCombination; W],
    r: &[LinearCombination; W],
) -> [LinearCombination; W] {
    let mut out = zero_lane();
    for i in 0..W {
        let (l, r, m) = cs.multiply(l[i].clone(), r[i].clone());
        out[i] = l + r - m * 2u32;
    }
    out
}

fn and<CS: ConstraintSystem, const W: usize>(
    cs: &mut CS,
    l: &[LinearCombination; W],
    r: &[LinearCombination; W],
) -> [LinearCombination; W] {
    let mut out = zero_lane();
    for i in 0..W {
        let (_l, _r, m) = cs.multiply(l[i].clone(), r[i].clone());
        out[i] = m.into();
    }
    out
}

// R[y,x]
const R: [[usize; 5]; 5] = [
    [0, 1, 62, 28, 27],
    [36, 44, 6, 55, 20],
    [3, 10, 43, 25, 39],
    [41, 45, 15, 21, 8],
    [18, 2, 61, 56, 14],
];

const RC: [u64; 24] = [
    1u64,
    0x8082u64,
    0x800000000000808au64,
    0x8000000080008000u64,
    0x808bu64,
    0x80000001u64,
    0x8000000080008081u64,
    0x8000000000008009u64,
    0x8au64,
    0x88u64,
    0x80008009u64,
    0x8000000au64,
    0x8000808bu64,
    0x800000000000008bu64,
    0x8000000000008089u64,
    0x8000000000008003u64,
    0x8000000000008002u64,
    0x8000000000000080u64,
    0x800au64,
    0x800000008000000au64,
    0x8000000080008081u64,
    0x8000000000008080u64,
    0x80000001u64,
    0x8000000080008008u64,
];

macro_rules! impl_state {
    ($($w:literal),+) => {
        $(impl_state!( IMPL $w );)+
    };
    (IMPL $w:literal) => {
        impl Default for KeccakState<$w> {
            fn default() -> KeccakState<$w> {
                use std::mem::{self, MaybeUninit};

                let mut state: [[[MaybeUninit<LinearCombination>; $w]; 5]; 5] =  unsafe {
                    MaybeUninit::uninit().assume_init()
                };

                for i in &mut state[..] {
                    for j in i.iter_mut() {
                        for k in j.iter_mut() {
                            *k = MaybeUninit::new(LinearCombination::default());
                        }
                    }
                }

                let state: [[[LinearCombination; $w]; 5]; 5] = unsafe {
                    mem::transmute(state)
                };
                KeccakState(state, 0)
            }
        }
    };
}

impl<const W: usize> KeccakState<{ W }>
where
    KeccakState<{ W }>: Default,
{
    fn next_bit_pos(&self) -> (usize, usize, usize) {
        let lane = self.1 / W;
        let y = lane / 5;
        let x = lane % 5;

        (x, y, self.1 % W)
    }

    fn xor_in<CS: ConstraintSystem, Bit: Into<LinearCombination> + Clone>(
        &mut self,
        cs: &mut CS,
        rate: usize,
        block: &[Bit],
    ) {
        for bit in block {
            assert!(self.1 < rate, "Invalid pointer state");

            let (x, y, l) = self.next_bit_pos();
            self.0[x][y][l] = xor_one(cs, self.0[x][y][l].clone(), bit.clone().into());
            self.1 = self.1 + 1;

            if self.1 == rate {
                self.permute(cs);
                self.1 = 0;
            }
        }
    }

    // TODO: I'd rather give the rate as const generic, but MIR doesn't like that yet.
    pub fn update<
        CS: ConstraintSystem,
        Bit: Into<LinearCombination> + Clone,
        Input: AsRef<[Bit]>,
    >(
        &mut self,
        cs: &mut CS,
        rate: usize,
        input: Input,
    ) {
        let input = input.as_ref();

        self.xor_in(cs, rate, input);
    }

    pub fn finalize<CS: ConstraintSystem>(
        &mut self,
        cs: &mut CS,
        rate: usize,
        delim: u8,
        output: usize,
    ) -> Vec<LinearCombination> {
        // Delimiter
        for i in 0..8 {
            let (x, y, l) = self.next_bit_pos();
            if ((delim >> i) & 1) == 1 {
                // invert
                self.0[x][y][l] = -self.0[x][y][l].clone() + 1u32;
            } // else: nop

            self.1 = (self.1 + 1) % rate;
        }

        // xor 0x80 at the end of the block
        self.1 = rate - 8;
        for i in 0..8 {
            let (x, y, l) = self.next_bit_pos();
            if ((0x80 >> i) & 1) == 1 {
                // invert
                self.0[x][y][l] = -self.0[x][y][l].clone() + 1u32;
            } // else: nop

            self.1 = (self.1 + 1) % rate;
        }

        self.permute(cs);
        assert_eq!(
            self.1, 0,
            "Pointer should be reset at 0 (rate={}), but is {}.",
            rate, self.1
        );

        // Extract `output` bits
        let mut out = Vec::with_capacity(output);
        for _ in 0..output {
            let (x, y, l) = self.next_bit_pos();

            out.push(self.0[x][y][l].clone());

            self.1 = (self.1 + 1) % rate;
            if self.1 == 0 {
                self.permute(cs);
            }
        }
        assert_eq!(out.len(), output);

        out
    }

    pub fn permute<CS: ConstraintSystem>(&mut self, cs: &mut CS) {
        let rounds: u32 = 12 + 2 * (W as usize).trailing_zeros();
        for i in 0..rounds {
            self.round(cs, i as usize);
        }
    }

    fn theta<CS: ConstraintSystem>(&mut self, cs: &mut CS) {
        let a = &mut self.0;

        //  C[x] = A[x,0] xor A[x,1] xor A[x,2] xor A[x,3] xor A[x,4],   for x in 0…4
        let mut c = [
            a[0][0].clone(),
            a[1][0].clone(),
            a[2][0].clone(),
            a[3][0].clone(),
            a[4][0].clone(),
        ];
        for x in 0..5 {
            for i in 1..5 {
                c[x] = xor(cs, &c[x], &a[x][i]);
            }
        }

        //  D[x] = C[x-1] xor rot(C[x+1],1),                             for x in 0…4
        for x in 0..5 {
            let d = xor(cs, &c[(x + 4) % 5], &rot(&c[(x + 1) % 5], 1));

            //  A[x,y] = A[x,y] xor D[x],                           for (x,y) in (0…4,0…4)
            for y in 0..5 {
                a[x][y] = xor(cs, &a[x][y], &d);
            }
        }
    }

    fn rho_pi_chi<CS: ConstraintSystem>(&mut self, cs: &mut CS) {
        let a = &mut self.0;

        // B[y,2*x+3*y] = rot(A[x,y], r[x,y]),                 for (x,y) in (0…4,0…4)
        let mut b = Self::default().0;
        for x in 0..5 {
            for y in 0..5 {
                b[y][(2 * x + 3 * y) % 5] = rot(&a[x][y], R[y][x]);
            }
        }

        // A[x,y] = B[x,y] xor ((not B[x+1,y]) and B[x+2,y]),  for (x,y) in (0…4,0…4)
        for x in 0..5 {
            for y in 0..5 {
                let intermediate = and(cs, &not(&b[(x + 1) % 5][y]), &b[(x + 2) % 5][y]);
                a[x][y] = xor(cs, &b[x][y], &intermediate);
            }
        }
    }

    #[doc(hidden)]
    pub fn round<CS: ConstraintSystem>(&mut self, cs: &mut CS, round: usize) {
        self.theta(cs);
        self.rho_pi_chi(cs);

        let a = &mut self.0;

        // ι step
        // A[0,0] = A[0,0] xor RC
        a[0][0] = xor_ct(&a[0][0], RC[round]);
    }

    pub fn zeroed() -> Self {
        Self::default()
    }
}

impl_state!(1, 2, 4, 8, 16, 32, 64);

macro_rules! impl_sponge {
    ($($name:ident $r:literal $c:literal $out:tt $sec:literal $mbits:literal $d:literal,)*) => {
        $(impl_sponge!(FUN $name $r $c $out $sec $mbits $d);)*
    };
    (FUN $name:ident $r:literal $c:literal unlimited $sec:literal $mbits:literal $d:literal) => {
        pub struct $name(KeccakState::<{($r + $c) / 25}>);

        impl $name {
            pub fn preimage(_input_len: usize, _output_len: usize) -> Vec<bulletproofs::r1cs::Variable> {
                unimplemented!()
            }
        }
    };
    (FUN $name:ident $r:literal $c:literal $out:literal $sec:literal $mbits:literal $d:literal) => {
        pub struct $name;

        impl $name {
            pub fn gadget_from_bytes<'a, CS: ConstraintSystem>(
                cs: &mut CS,
                input_len: usize,
                witness: impl Into<Option<&'a[u8]>>,
            ) -> Result<(Vec<bulletproofs::r1cs::Variable>, Vec<bulletproofs::r1cs::LinearCombination>), failure::Error>
            {
                let mut bits = Vec::with_capacity(input_len * 8);
                let witness = witness.into().map(AsBits::bits::<Lsb0>);

                for i in 0..input_len {
                    for j in 0..8 {
                        let bit = witness.as_ref().map(|w| {
                            ((w[i*8 + j] as u32).into(), (!w[i*8 + j] as u32).into())
                        });

                        let (bit, neg, mul) = cs.allocate_multiplier(bit)?;

                        // Constrain bit to [0, 1]
                        cs.constrain(mul.into());
                        cs.constrain(bit + neg - 1u32);

                        bits.push(bit);
                    }
                }

                let hash = Self::gadget(cs, &bits);
                Ok((bits, hash))
            }

            pub fn gadget<CS: ConstraintSystem, InputItem, Input>(
                cs: &mut CS,
                witness: Input,
            ) -> Vec<bulletproofs::r1cs::LinearCombination>
                where InputItem: Into<LinearCombination> + Clone,
                      Input: AsRef<[InputItem]>,
            {
                let mut state = KeccakState::<{($r + $c) / 25}>::zeroed();

                state.update(cs, $r, witness.as_ref());
                state.finalize(cs, $r, $d, $out)
            }
        }
    };
}

// Table comes literally from https://keccak.team/keccak_specs_summary.html,
// save for the rustification of names and addition of commas.
impl_sponge! {
    Shake128    1344    256     unlimited   128     1111    0x1F,
    Shake256    1088    512     unlimited   256     1111    0x1F,
    Sha3_224    1152    448     224     112     01  0x06,
    Sha3_256    1088    512     256     128     01  0x06,
    Sha3_384    832     768     384     192     01  0x06,
    Sha3_512    576     1024    512     256     01  0x06,
    CShake128   1344    256     unlimited   128     00  0x04,
    CShake256   1088    512     unlimited   256     00  0x04,
}

pub fn keccakf_gadget<CS: ConstraintSystem>(
    cs: &mut CS,
    inputs: Vec<Scalar>,
) -> Vec<LinearCombination> {
    let mut state = KeccakState::<64>::default();
    let mut inputs = std::collections::VecDeque::from(inputs);
    for y in 0..5 {
        for x in 0..5 {
            for l in 0..64 {
                state.0[x][y][l] = inputs.pop_front().unwrap().into();
            }
        }
    }

    debug_assert_eq!(inputs.len(), 0);

    state.permute(cs);

    let mut out = Vec::with_capacity(1600);
    for y in 0..5 {
        for x in 0..5 {
            for l in 0..64 {
                out.push(state.0[x][y][l].clone());
            }
        }
    }
    debug_assert_eq!(out.len(), 1600);

    out
}

#[cfg(test)]
mod tests {
    use super::*;

    use bulletproofs::r1cs::*;
    use bulletproofs::{BulletproofGens, PedersenGens};
    use curve25519_dalek::scalar::Scalar;
    use failure::*;
    use rayon::prelude::*;

    #[cfg(debug_assertions)]
    fn parse_lane<const W: usize>(hex: &str) -> Result<[LinearCombination; W], Error> {
        let mut out = zero_lane();

        let bytes = hex::decode(hex)?;
        let bits = BitSlice::<Lsb0, _>::from_slice(&bytes);
        // ensure!(bits.len() == 16, "Input len does not match {}", W);

        for (i, bit) in bits.iter().enumerate() {
            out[i] = if *bit { Scalar::one() } else { Scalar::zero() }.into();
        }

        Ok(out)
    }

    #[test]
    #[cfg(debug_assertions)]
    fn rho_pi_chi() -> Result<(), Error> {
        let pc_gens = PedersenGens::default();
        let bp_gens = BulletproofGens::new(8192, 1);

        let mut transcript = merlin::Transcript::new(b"keccak-f");
        let mut prover = Prover::new(&pc_gens, &mut transcript);

        let mut state = KeccakState::<64>::zeroed();

        let input = "1B986301005041003124166B007A000110D4F3D000504100221C950000E600011A7413BA009C00001C986309005041003124C66B00EA000110D4F0D000504100241C8508000600011A74D0BA00EC000012986300005041003128066B000A000110D4F0D1005041002A10850000E600011A7410BA00EC00001C846361005041003164066B000A000110C8F0D00050C100245C856000E600011A7410BA00EC80001C986301005641003124066B000A40010CD4F0D000564100241C850000E60001067410BA00EC4000";
        let input = BitVec::<Lsb0, _>::from(hex::decode(input)?);
        for y in 0..5 {
            for x in 0..5 {
                for l in 0..64 {
                    let pos = x * 64 + y * (5 * 64) + l;
                    let bit = if input[pos] { 1u32 } else { 0u32 };
                    state.0[x][y][l] = bit.into();
                }
            }
        }

        let out = "1898630102D0E184AC26A08AB9004F228F86811306AEA0BD1738E284AB404DC0B4868113942E0259F0CE0C22C25109026E0DE041278D8F0C11D20219C08A020200094021227187CC8F1ABE5BC5860A40AE492E9600FE0102001E413CAE00B890C9000234240F015862527886BAF0EC8205056148BA055448E4C0A0C88C9BD00560001D6090E9393E1841A0182A012904800008B478F3D0A91C851504A60028B655F54E3400592A080C42848203B000D559906E2C010D3A08C2658492006C18C74E9218AC02A80017";

        state.rho_pi_chi(&mut prover);

        // Big endian NIST output for the above.
        let out = BitVec::<Lsb0, _>::from(hex::decode(out)?);

        // First lane
        for y in 0..5 {
            for x in 0..5 {
                for l in 0..64 {
                    let pos = x * 64 + y * (5 * 64) + l;
                    let bit = if out[pos] { 1u32 } else { 0u32 };
                    prover.constrain(state.0[x][y][l].clone() - bit);
                }
            }
        }

        let _proof = prover.prove(&bp_gens)?;
        Ok(())
    }

    #[test]
    #[cfg(debug_assertions)]
    fn theta() -> Result<(), Error> {
        let pc_gens = PedersenGens::default();
        let bp_gens = BulletproofGens::new(8192, 1);

        let mut transcript = merlin::Transcript::new(b"keccak-f");
        let mut prover = Prover::new(&pc_gens, &mut transcript);

        let mut state = KeccakState::<64>::zeroed();

        let test_vectors = vec![
            ("A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A30000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", "0000000000000000A3A3A3A3A3A3A3A3E4E4E4E4E4E4E4E4E4E4E4E4E4E4E4E400000000000000000000000000000000A3A3A3A3A3A3A3A3E4E4E4E4E4E4E4E4E4E4E4E4E4E4E4E400000000000000000000000000000000A3A3A3A3A3A3A3A3E4E4E4E4E4E4E4E4E4E4E4E4E4E4E4E400000000000000000000000000000000A3A3A3A3A3A3A3A3E4E4E4E4E4E4E4E44747474747474747A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3000000000000000047474747474747474747474747474747A3A3A3A3A3A3A3A3"),
            ("0700000000000000000010000070000000000300000000000600100000000000000003000070000000000008000000000000C00000E0000000000000000000000000000800E000000000C000000000000E00000100000000000C00000000000000000001000000000E0C0000000000000000000000000000001C0060000000000040000000000000001C00000000800000400060000000000000000000008000000000000006000000000000000040001C0000000006000000000000000000001C00000000004000", "1B986301005041003124166B007A000110D4F3D000504100221C950000E600011A7413BA009C00001C986309005041003124C66B00EA000110D4F0D000504100241C8508000600011A74D0BA00EC000012986300005041003128066B000A000110D4F0D1005041002A10850000E600011A7410BA00EC00001C846361005041003164066B000A000110C8F0D00050C100245C856000E600011A7410BA00EC80001C986301005641003124066B000A40010CD4F0D000564100241C850000E60001067410BA00EC4000"),
        ];
        for (input, out) in test_vectors {
            let input = BitVec::<Lsb0, _>::from(hex::decode(input)?);
            for y in 0..5 {
                for x in 0..5 {
                    for l in 0..64 {
                        let pos = x * 64 + y * (5 * 64) + l;
                        let bit = if input[pos] { 1u32 } else { 0u32 };
                        state.0[x][y][l] = bit.into();
                    }
                }
            }

            state.theta(&mut prover);

            // Big endian NIST output for the above.
            let out = BitVec::<Lsb0, _>::from(hex::decode(out)?);

            // First lane
            for y in 0..5 {
                for x in 0..5 {
                    for l in 0..64 {
                        let pos = x * 64 + y * (5 * 64) + l;
                        let bit = if out[pos] { 1u32 } else { 0u32 };
                        prover.constrain(state.0[x][y][l].clone() - bit);
                    }
                }
            }
        }

        let _proof = prover.prove(&bp_gens)?;
        Ok(())
    }

    #[test]
    #[cfg(debug_assertions)]
    fn rot_and_not_xor() -> Result<(), Error> {
        let pc_gens = PedersenGens::default();
        let bp_gens = BulletproofGens::new(8192, 1);

        let mut transcript = merlin::Transcript::new(b"keccak-f");
        let mut prover = Prover::new(&pc_gens, &mut transcript);

        // Test 16-bit lanes for rot3
        let test_vectors = vec![
            ("0000", "0000", "0000", "FFFF", "1111"),
            ("0100", "0800", "0100", "FEFF", "1011"),
        ];

        let and_const = parse_lane::<16>("1111")?;
        let xor_const = parse_lane::<16>("1111")?;

        for (input, rot_out, and_out, not_out, xor_out) in test_vectors {
            let input = parse_lane::<16>(input)?;
            let rot_test = parse_lane::<16>(rot_out)?;
            let and_test = parse_lane::<16>(and_out)?;
            let not_test = parse_lane::<16>(not_out)?;
            let xor_test = parse_lane::<16>(xor_out)?;

            let rot_out = rot(&input, 3);
            let and_out = and(&mut prover, &input, &and_const);
            let not_out = not(&input);
            let xor_out = xor(&mut prover, &input, &xor_const);
            for (inp, out) in rot_out
                .iter()
                .zip(rot_test.iter())
                .chain(and_out.iter().zip(and_test.iter()))
                .chain(not_out.iter().zip(not_test.iter()))
                .chain(xor_out.iter().zip(xor_test.iter()))
            {
                prover.constrain(inp.clone() - out.clone());
            }
        }

        let _proof = prover.prove(&bp_gens)?;
        Ok(())
    }

    #[test]
    fn round() -> Result<(), Error> {
        let pc_gens = PedersenGens::default();
        let bp_gens = BulletproofGens::new(8192 * 32, 1);

        let mut transcript = merlin::Transcript::new(b"keccak-f");
        let mut prover = Prover::new(&pc_gens, &mut transcript);

        let mut state = KeccakState::<64>::zeroed();
        // NIST sha3-224 test vector
        // state.0[0][0] = 0000000000000006;
        // state.0[2][3] = 8000000000000000;
        state.0[0][0][1] = Scalar::from(1u32).into(); // bits are little endian.
        state.0[0][0][2] = Scalar::from(1u32).into();
        state.0[2][3][63] = Scalar::from(1u32).into();

        let round_vectors = vec![
            "0700000000000000000010000070000000000300000000000600100000000000000003000070000000000008000000000000C00000E0000000000000000000000000000800E000000000C000000000000E00000100000000000C00000000000000000001000000000E0C0000000000000000000000000000001C0060000000000040000000000000001C00000000800000400060000000000000000000008000000000000006000000000000000040001C0000000006000000000000000000001C00000000004000",
            "9A18630102D0E184AC26A08AB9004F228F86811306AEA0BD1738E284AB404DC0B4868113942E0259F0CE0C22C25109026E0DE041278D8F0C11D20219C08A020200094021227187CC8F1ABE5BC5860A40AE492E9600FE0102001E413CAE00B890C9000234240F015862527886BAF0EC8205056148BA055448E4C0A0C88C9BD00560001D6090E9393E1841A0182A012904800008B478F3D0A91C851504A60028B655F54E3400592A080C42848203B000D559906E2C010D3A08C2658492006C18C74E9218AC02A80017",
            "30F100A24B7E1EEFCFC13BCF3CFDD64E392138B3E044F8919311F4AB18ECDF593739DB35A04FF5631F16F1A615AA721B3FAF3BC475D76911A05C6B2EEE7022C429C5F94B100E677853756187C6BD9833720430FD59E478A061F46E8941E9FC878862131AB3A9BBCDCCC4F7B2DB557DC9723B1F51B6C5A6D3537B4E0523472D15FA54EB964FFEFC069F4779848481892658D87A1C6885AF0454E869DC45F81E0FF45FAD87DD71479A58DE523A8CEFA3E4AF35007E9CEFA71C967C43CF36D841055A09C34655856D48",
            "E6DEB2021A8049DF528668AB784208DA581CF3C16FD5D6881CF025AB9F7AA72EFEE5C94DB1E4AA38A318273B02750DD662C694C2752DBFC368998DC476D45D11D582FB9623536829C9CDAA4A7D970F97893CD3AD15194C469B61DA8820D7591B6D26ECB91B275BCEB50904F7A70B5F4918EB20E4B34A3F6206356291332033AA39D37CBB8E1202267C60A7FB0AE227F023F7CF5F95D43A3B0525543DB67826830F168B070EC36C9E82E5A25B3DECB38F17619A9C723F806C59FAC05A4B9BFECD45666BF4CE1489ED",
            "2A80AE2B2C588198B6834DE5E2B1AC4D315970253D4C2BB7D0D45ABA2B033A9E8A6FB33C786DE69C9FA29B785B3C71209EFE5CD90332484C3D84EC827CCD12C86FC14B475438D329010E1D644CF6661B713730357AD2016B3A94B8F1774A99F2A5A28D985E0D2486AD99D0C297542ADF9E27CD639AB046ECF288221027893A20B1C36A7CD84CBF11CBCDB599F4D5FB044895924DFBAAB53B59C59D171A85C1BAD670F8D70812CD15F0C6EC3A28AA72969F6B95EA560D1F5521085077A390F689A9664D91627F3E82",
            "A9FD482433D7B8032C99C1803B0F0796C9804AA450AF6023AABFA46F935AB79F2AE7212074FD2594530E4BCD6E0D90B887D6A042DD3EA7CA93C587F22E4AC2AAFA0318098D58F55B0B44C70F3AD397715EFA42BD626AD9F3EA045EF1C0BC9228155DF323171DAE3FA5E2AEA875E43ACD52BA4676D1E44F7C04D27395097FD0EBAD561CA287FDB83F0F8168A101CF3EA1A1B3A405460DA9C11322C891A7FCAE02F7D451AC16E0424D8C463820E7CD1AA91AB51D6CBD0AE6BA367D18263A21CDC4A4696A0F21B50926",
            "250B7E38AFA6E25AF76CE580B126140CFDD63B88280C6CA10ED9664EC05D84E7E5195BFF50DF49C81F841DD767A69A5CF5116B98B2EAFA56DE9A7A43809B1495FD9125614BE2BF351769A9A8E2EC48EAB7AF79F671B5BD6294C5E730C6E1984A9BB774AFC6C73D25CAD78D98C762F875A4D4CEC31265487E18DB679BDD41BB782CC2AE5E69E5EA1741704C2DBAA7A263C205725FD2269CEB525388D83A085762FB7EA33948646AF585939AA0104AA21CB80688EA462B18E623008E5729035C8ABCC42873816FC273",
            "D062D98A5DB65BA204A567C60CDB6164B2E8A2241025534272392DD47967D00EFE4D0CC1C9E77B65BE2F5CD4A3FB0AC171E223800F30DE638C78A0AA8909E04C6230046FC1EAB26A47DB75A95E2BE0CE8D8DD5DF71977EDD469493D1FA402AB2B157C18E79A2A2C66693C28670C184E42D2BF67C98AAAA4214DEA30E6E86BF7D1038BF651A9DB2638BE3E1E03626A2DF58E20754F8660E0BAA6793FC2D4285109CCA00C86F5F37CD2DF67F732CD048E6A6FF4A73D450A2D10CBCEBBC9CEFE50C8205424565AC2D53",
            "D4B6EE3747F21FAE814E38EC05D6B51864CF73BC6E9213DFB100D3F5550ABBAE5BFA4D12F84BB58BBF29D51DDA01DF60AD6AE5337F71CE9C240C2387C3965EAD3100075D74DC62EB5D0F2A29A9FFDA2356804F5E00794C9754633B6FE09719A1174D1FF6AF2FC3D29BDE0031A9DD82434C6CC09C209BCC4A6BC1ED140A27DEC49EB7EB8CDC80DCB7D2AEC8C7C144C268C8B1E8FDF445902D4C1877B1E7479FE56F8B4AD3CDF60A5CEA0150A502C9A8F81F04C47C7C6A231A31E60F714E397EEA8D34676D9C86C493",
            "EFA922038D0EDFC619372F829617EB85094E81F8BDB67E7D94009AB29FB0E625B822D205ECD6E65033EF4AF7FBF04DFBA20DC78CB1E9D5A3A67AFE3BB889CAB6521B1F1E1B5A76F41ADC7B91ABF4CBDE12A5DA4F610D07FFF77E70C40C580692511AB03C3A505824A65931E68364C3C83A69D1739719BAA951A609EFCFD60EC95855B106FA48774D32400D7841501EEB8D44083477BCF62A2D7240E5E0C080B83B09D383D7413C6DAF15BCFF0A6F001FF736AD0C17EA351EDA1AB51748702CCA1E6338849B5ABAE6",
            "12AC804A108CCDD3D3B566C64D4B155F739E2C6BB0393D8D656F18BEC2C0F64A5BA4EE69F7133D785AF446FD9F54770247281A162F6AD54C7657A07AD167E151CA7A86F0C30323E7C141BD9FD737ED89D2B431879D0887B7A0DCE497D445EBE0D69F4A94DDB6E3F208C5D71D32F7CC671368DF03848F49CFE29B29233D1D49C6404686C33402A2664A17885A35F059AF817F5237DD01E5E24EFF44B226B22597EFDC402A4DEE785AA39CEC5256ED8F5DBBD9CB3ECEA7D6341EBCA97E7D725B0CF585A7E87A962435",
            "B33D8B9445965ADFC8AA30A3DB5D59F7DC4AA15DD7EB99211D88CF700015613726BB0CB3D029EAC55B54FD0CD6A584D02190459FB93B2D4BFC2ADB3D6206955C2675BFCF697FB63BA32DABCA4796B8BE8856A2EB403EF59D34F5930D7FA8EAB81F9896AF46990549455D9DC39B7D339EC1FBD1758E6378FA9511556487AE2300E7DA4EFE7A54C01E04A99C4FE5E7E0191D9772718FD960FAA03FE941A29A40A544896EE35E4D1F2B447EAFBAC12C1500316CA3FB65021E19735800BB85F2A346C57B65B9EF522A77",
            "D8C6FF0A1DEFAF81805B78573A2B4CC6BD05F05678996ADDCA42BF82EB2CB11A4893ED0329B9D0E8652AE07832AE3C3F07BE0CCCB01F414AA24AD4E6E71A11DA60FF121196DFE4E42F5A7DD9262962ABD2EFEA424B12C24DF7BAA0D953466AEC88A9784805C4A213231F8DA9184F3A46243360E269BB2B7149174DA4CA2062A6B3E630F3AB9BD3B4A545A23932981662BA3FEB2E420B7B5097EF190E6A4C4C3B700E273130104ED16DC200986FE044349730513E63D9CE48950ECBF9CF999CDB8A4F12AF7560A765",
            "C02F48D0DEBEB78B6C0B6BE762B965C8E949863EAA548BEF8AFBBAD457AE496391C7331D6A0368A2CE54473B1D6E6E343C407D4AAE3542160F9EC827642800A27F17F598DDCF7F1F8E8CBE399741F4B2B0E3702A56AAEE7C17904D4E4D9D7742FEDDC4FD22E88141089078358CBD855677E3C8C9D3ECDE15C71A899EA691AFB5B0E54B264B73660F50AC0BF19D72A1DC7DF22B8401E57D07313757D7D81F2D84550CC40169A4E8D530A52C17F980B5EC21B3A9272D3EC02FCC102FC07316743B6F5C29DD1939EF18",
            "5D53093157FAA16C40E7BC51BA3246FEA45A21628859E599B52B392FCCAE4F1D2C963ABBB7537A707DA6F2EAFB668A906A29BBD988465B2D4AF8FA89F526C914F517951BD8EC21280D4AF2D05E78D7CAE7D522A305CD962928744EE4B33FC95DC31737FB0F4C939FD436B419569DAE7FA7CD7AA7F928E4B7BD15D0B7BC926315576E962F4D077BA6FF90EB4C87AF5EC93F420602880C2373C1583EF89E1BEF4809F3BBA53C0B4FED5901A23E07C425ADA08B2E666AB1310F3A253D11B6FDCABC7C0338BFEF7CE9D4",
            "FC30C610A0AF3B3658F449AEFB423FB1D0A543E49275FFA0FA9C14474F62CE3CB88E9B150915B4F998B6F80E8FC54398DE0D23F7039B9B07BC1E09BF34CF609E26429B5059A5BAFEF5F9E39DEB161D973CBBB81D4309EEE460AFD568A0A1F825B18817A7D9A9630E3E95ABEA19B065E117DCE86C70E33FFB4A4CC9CFB071A18AC93041EDF0419CA67BA7AE1FFA13B2E023A02B5A4A455BFD9E4C3CCA58801E0E829486ED5E2B9FC31FFE9FC28E2086D996034B777428069E8F070E2B301C657E28051783C048319C",
            "71E2FE41F7B10E6BDD27213F06F1F0ADDBA7E0DF32A66CAE07EC59D85818D0DD5B839176291B0C912308DF21C124D5FE123683680D008298C94D064CAF50D89E377DE8847417EBA7632E05E6ABCE6367C144E2E8B414463C64C4FC586E9EB79F91BD54BD771B6A7EF9FDA9EB012DD379A4F2290AB359414621586432CF49F1C58DD8043532C91ED0EB83963181560347573CF951EC8CDBABCD65ED91745F395F8E7998D897F019DB24464FD66FC25F25D1135720BA9DBDA5FFC4D7F8AB423D22BD55DB2ED723BF3E",
            "3193773498893CC14553977573E561B2FFB935AC8FCD70EA4094E28E94330863D232E9F4472BC35DF0DC2BD576EDE35A443C38128FC15B3F4A431922B2E2F593C354A356B10C1B5907CE95DCA284D438E40A9667F4818F2D1F92899BE732BB4491C7FD336F8D2FC9BBDC8FE253A2C959F6569319E584A071F769AF8B4A8166B520C054675EBCFDB3501D989D1055A5816E423671BD307455D430DC5A3410A4DD4E01E491A65455A9A50FF8E06EEC9720C836FF5A581294109F794023F691161B1F5F456A17F52A29",
            "A5931CC6E809C7B8BCD4D2B3DA24EB53DCB010AD0104C43F63B072E2BB2CE3A805105BF9FFCA3969FE45AF58969C6DCFA99A260FAA5130AFFB79A5998C7827C15385FDE8479B52B579B35019E8A7213250E4C4BD63BF6AB2E4B4C37AEA7CFF5BFE91732D93B2351EA621F85E6BC56F7E4CE248869A9ED85231E52413788F0A363AF0EED0D0CA173457F5181881F37A7D0F200821A09BA038E60B914A68BE6FF2BD73DB88F3FE7BBA3F1815E67B295E5CA09B3C4B360B9930C3BE259284F7600347315052BA5B0862",
            "4FC50ED8D50B236F463126B61A3765879508AEFF09D3B45CF0DE56210C17C0B57FC8F45A0C018D3C6FC349F75E4DCACD609BDD8BB7F564A04DF1DD16FDF152A0F69FC37EEB401C832CA73E0E2DABD92BDC39FC3D7D2D30E0EE7C177FB534AE9FBA6DD672C134FA19F9BAE0D2E5D657133F321575848DB7EE1B3AF2B18887508BB9EA1E414CBA12794ABBD43F93B1BCDD15A999F522170B93DB03209389239CBEAF6FCAB6D97CA510B284AF84E28673538DC07ED3D1E090033AA5022C6292577D95C34225FFFE0411",
            "89B108906FA4D53D10E6BF86F91DE71CC09882F7E4827EB15CB888DFEE9D54B626DC82363AD6DEB4D9F02233A42232DB6831D596AA7552A361EBE90C0912BA3E509F80D46A5AF74CDDD76A3B1419DBC0882875BBB65A4373EE1149DFEE768E5CC0250035CC128D26479A745B68C544026D0E21087A1E1041E1890DFF49E521092A62D2B203F4A2D5D3A247AF12E2814A6E651C36F3EFD0397650B6B28288383FFD930F744C9675A2F573937C492DAF8A87C6D80C47BDDA6E6D7DD36DB379ACEFC38E5CF51C0B74F9",
            "A1540ED14FFF611BE4EB0B76B84957C099B9E3373DCCC03D2A4A64A3B6FCDAE445C3015525765CB0928FDD6144AB4F20152CF97111E49118E469D3DFC67CF7D25C3BB610AC2FB0CC669B5B13442E6BC536A2D7DCEB96646133981D2E0578FDAC7AC8F76A1C8EC53C16F413BBCFDF061204A93CBAA3FA3BA303252DC7BDF4597907A8EB0F35521A3ED82519B05FFA0D74C255BC3B578C0626FDB8C653010E841B54C301C81A8F3B50E0510E4F950ACBD29213E3E5E91626BC7B9B100216BBEF93E38987D5E5C3E838",
            "D3581CF968AAD6DE3A7B85FBC6CDFDBE06D784ABF9A5731D60AB827F8E1B03D44797A4792E9D96020B0F5D47C07600B2A4C368E92EA79E7425362D33220E8FAC5D23E26910CD7815B4A4641DA7DD96C27B73B227B9EA88A9C59CDFAAB5A1A615AE6DD455FD42A00D3280EDB894334F51BF5EDE3CC990F0ACF8498EF6306C16FCFAF7ADCD6E66D89EA0ED6F3C9EA5D5DC3E78EB4EE4D42C3831C40002B9E48247A7FE4C68E225E8EE12E09B508B170E8E8694A97761206EC0DFA40F9F7BE3DA99EA48E399D78D5648",
            "6B4E03423667DBB73B6E15454F0EB1ABD4597F9A1B078E3F5B5A6BC76DB1CD4FA555EB6E33A211E040FC205BC618F503EA9135F2E7C6E45A5AADB357D5314CA8FDC823D216098AF3D2C09A55DFC19D2578C7C2A328C415700136BB3E28CCCC067C8B278FE95F4187D926B5087BFBCD512F5F75FDF7AEB3A082B76083207352E8CCA95DE8E8FE88379DFD89A3A312E6222018873BDE70067DFE47858F69F877587E7F5EED8A7A3F6ED24134C86BEBB1F926CF6879F41356EAE44D40BF1038E7FBE46256698C4682E1",
        ];

        assert_eq!(round_vectors.len(), 24);
        for (round, out) in round_vectors.into_iter().enumerate() {
            state.round(&mut prover, round);

            // Big endian NIST output for the above.
            let out = BitVec::<Lsb0, _>::from(hex::decode(out)?);

            // First lane
            for y in 0..5 {
                for x in 0..5 {
                    for l in 0..64 {
                        let pos = x * 64 + y * (5 * 64) + l;
                        let bit = if out[pos] { 1u32 } else { 0u32 };
                        prover.constrain(state.0[x][y][l].clone() - bit);
                    }
                }
            }
        }

        let _proof = prover.prove(&bp_gens)?;
        Ok(())
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore)]
    fn keccakf_gadget_vs_tiny_keccak() -> Result<(), Error> {
        // NIST test-vectors: (0, 0) -> (1, 0) -> (2, 0), ...
        // lanes are in little endian
        let test_vectors = vec![
            "0600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            "1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            "1000000000000000000000000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
        ];

        eprintln!("Starting generators");
        let pc_gens = PedersenGens::default();
        let bp_gens = BulletproofGens::new(32 * 8192, 1);
        eprintln!("Starting tests");
        test_vectors
            .par_iter()
            .try_for_each(|input_str| -> Result<(), Error> {
                let input = hex::decode(input_str)?;
                assert_eq!(input.len(), 200);

                let mut tiny = [0u64; 25];
                for i in 0..25 {
                    for j in 0..8 {
                        tiny[i] |= (input[i * 8 + j] as u64) << (8 * j);
                    }
                }

                tiny_keccak::keccakf(&mut tiny);
                // Compute permutation output
                let tiny: Vec<u8> = tiny
                    .iter()
                    // .rev()
                    .flat_map(|i| i.to_le_bytes().to_vec())
                    .collect();

                println!("    inp: {}", hex::encode(&input));
                println!("    out: {}", hex::encode(&tiny));

                let inputs: Vec<Scalar> = BitSlice::<Lsb0, _>::from_slice(&input)
                    .into_iter()
                    .map(|bit| Scalar::from(if *bit { 1u32 } else { 0 }).into())
                    .collect();

                assert_eq!(inputs.len(), 1600);

                // Proof
                let proof = {
                    let mut transcript = merlin::Transcript::new(b"keccak-f");
                    let mut prover = Prover::new(&pc_gens, &mut transcript);
                    let out = keccakf_gadget(&mut prover, inputs.clone());
                    assert_eq!(out.len(), 1600);
                    for (tiny_bit, output_bit) in BitSlice::<Lsb0, _>::from_slice(&tiny)
                        .iter()
                        .zip(out.into_iter())
                    {
                        prover
                            .constrain(output_bit - Scalar::from(if *tiny_bit { 1u32 } else { 0 }));
                    }

                    prover.prove(&bp_gens)
                }?;

                let mut transcript = merlin::Transcript::new(b"keccak-f");
                let mut verifier = Verifier::new(&mut transcript);

                let out = keccakf_gadget(&mut verifier, inputs);
                assert_eq!(out.len(), 1600);
                for (tiny_bit, output_bit) in BitSlice::<Lsb0, _>::from_slice(&tiny)
                    .iter()
                    .zip(out.into_iter())
                {
                    verifier.constrain(output_bit - Scalar::from(if *tiny_bit { 1u32 } else { 0 }));
                }
                verifier.verify(&proof, &pc_gens, &bp_gens)?;

                Ok(())
            })?;

        Ok(())
    }

    macro_rules! hash_test {
        ($($testname:ident $hash:ident $type:ident [$($input:literal => $output:literal,)*],)*) => {
            $(hash_test!(TEST $testname $hash $type [$($input => $output,)*]);)*
        };
        (TEST $testname:ident $hash:ident $type:ident [$($input:literal => $output:literal,)*]) => {
            #[test]
            #[cfg_attr(debug_assertions, ignore)]
            fn $testname() -> Result<(), Error> {
                eprintln!("Starting generators");
                let pc_gens = PedersenGens::default();
                let bp_gens = BulletproofGens::new(64*8192, 1);
                eprintln!("Starting tests");

                let tests = vec![
                    $( ( $input, $output ) ),*
                ];

                tests.par_iter().try_for_each(|(input_hex, output_hex)| -> Result<(), Error> {
                    let input = hex::decode(input_hex)?;
                    let output = hex::decode(output_hex)?;
                    let len = input.len();

                    let proof = {
                        let mut transcript = merlin::Transcript::new(b"keccak-f");
                        let mut prover = Prover::new(&pc_gens, &mut transcript);

                        let (_input_bits, output_bits) = $hash::gadget_from_bytes(&mut prover, input.len(), input.as_ref())?;
                        assert_eq!(output.len() * 8, output_bits.len());

                        for (test, out) in BitSlice::<Lsb0, _>::from_slice(&output).iter().zip(output_bits.iter()) {
                            let test: u32 = if *test { 1 } else {0 };
                            prover.constrain(out.clone() - test);
                        }

                        prover.prove(&bp_gens)
                    }?;

                    drop(input);

                    // Verify proof
                    let mut transcript = merlin::Transcript::new(b"keccak-f");
                    let mut verifier = Verifier::new(&mut transcript);

                    let (_input_bits, output_bits) = $hash::gadget_from_bytes(&mut verifier, len, None)?;
                    assert_eq!(output.len() * 8, output_bits.len());

                    for (test, out) in BitSlice::<Lsb0, _>::from_slice(&output).iter().zip(output_bits.iter()) {
                        let test: u32 = if *test { 1 } else {0 };
                        verifier.constrain(out.clone() - test as u32);
                    }

                    ensure!(verifier.verify(&proof, &pc_gens, &bp_gens).is_ok(),
                        "Could not verify {}({:?})=?={:?}", stringify!($hash), input_hex, output_hex);
                    Ok(())
                })?;

                Ok(())
            }
        };
    }

    hash_test! {
        sha3_224 Sha3_224 HASH [
            "" => "6B4E03423667DBB73B6E15454F0EB1ABD4597F9A1B078E3F5B5A6BC7",
            "61" => "9E86FF69557CA95F405F081269685B38E3A819B309EE942F482B6A8B",
            "54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f67" => "D15DADCEAA4D5D7BB3B48F446421D542E08AD8887305E28D58335795",
            // exactly the rate.
            "4d61792074686520676f6473207761746368206f76657220796f752c20616e64207468657920636f6e7369646572207768617420796f7527766520646f6e652e20427574206e6f7720796f752776652068696464656e20617761792c20746f206761696e20796f757220737472656e6774682e204465657020696e206120636176652c20796f757220706f7765722068" => "57D5A7AB9778BE70EDEDC5800C1E3793DBD9726C99B4E24C71AB3B3D",
            // A bit more than the rate
            "4d61792074686520676f6473207761746368206f76657220796f752c20616e64207468657920636f6e7369646572207768617420796f7527766520646f6e652e20427574206e6f7720796f752776652068696464656e20617761792c20746f206761696e20796f757220737472656e6774682e204465657020696e206120636176652c20796f757220706f776572206861732072656761696e65642e" => "809F98DB23D820654789AF9E78A1A01386F7FF9CA1999901E2958F23",
        ],
        sha3_256 Sha3_256 HASH [
            "" => "a7ffc6f8bf1ed76651c14756a061d662f580ff4de43b49fa82d80a4b80f8434a",
            "61" => "80084BF2FBA02475726FEB2CAB2D8215EAB14BC6BDD8BFB2C8151257032ECD8B",
            "54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f67" => "69070DDA01975C8C120C3AADA1B282394E7F032FA9CF32F4CB2259A0897DFC04",
        ],
        sha3_384 Sha3_384 HASH [
            "" => "0C63A75B845E4F7D01107D852E4C2485C51A50AAAA94FC61995E71BBEE983A2AC3713831264ADB47FB6BD1E058D5F004",
            "61" => "1815F774F320491B48569EFEC794D249EEB59AAE46D22BF77DAFE25C5EDC28D7EA44F93EE1234AA88F61C91912A4CCD9",
            "54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f67" => "7063465E08A93BCE31CD89D2E3CA8F602498696E253592ED26F07BF7E703CF328581E1471A7BA7AB119B1A9EBDF8BE41",
        ],
        sha3_512 Sha3_512 HASH [
            "" => "A69F73CCA23A9AC5C8B567DC185A756E97C982164FE25859E0D1DCC1475C80A615B2123AF1F5F94C11E3E9402C3AC558F500199D95B6D3E301758586281DCD26",
            "61" => "697F2D856172CB8309D6B8B97DAC4DE344B549D4DEE61EDFB4962D8698B7FA803F4F93FF24393586E28B5B957AC3D1D369420CE53332712F997BD336D09AB02A",
            "54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f67" => "01DEDD5DE4EF14642445BA5F5B97C15E47B9AD931326E4B0727CD94CEFC44FFF23F07BF543139939B49128CAF436DC1BDEE54FCB24023A08D9403F9B4BF0D450",
        ],
    }
}
