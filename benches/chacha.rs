#![feature(const_generics)]

use smoking_gun::aead::chacha::*;

use bulletproofs::r1cs::{ConstraintSystem, Prover, Verifier};
use bulletproofs::{BulletproofGens, PedersenGens};

use criterion::criterion_group;
use criterion::criterion_main;
use criterion::Criterion;
use criterion::{BenchmarkId, Throughput};

const RFC_7539_KEY: [u8; KEY_LEN] = [
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
];
const RFC_7539_NONCE: [u8; NONCE_LEN] = [
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4a, 0x00, 0x00, 0x00, 0x00,
];

fn chacha_apply_stream(c: &mut Criterion) {
    let ciphertext = [
        0x6e, 0x2e, 0x35, 0x9a, 0x25, 0x68, 0xf9, 0x80, 0x41, 0xba, 0x07, 0x28, 0xdd, 0x0d, 0x69,
        0x81, 0xe9, 0x7e, 0x7a, 0xec, 0x1d, 0x43, 0x60, 0xc2, 0x0a, 0x27, 0xaf, 0xcc, 0xfd, 0x9f,
        0xae, 0x0b, 0xf9, 0x1b, 0x65, 0xc5, 0x52, 0x47, 0x33, 0xab, 0x8f, 0x59, 0x3d, 0xab, 0xcd,
        0x62, 0xb3, 0x57, 0x16, 0x39, 0xd6, 0x24, 0xe6, 0x51, 0x52, 0xab, 0x8f, 0x53, 0x0c, 0x35,
        0x9f, 0x08, 0x61, 0xd8, 0x07, 0xca, 0x0d, 0xbf, 0x50, 0x0d, 0x6a, 0x61, 0x56, 0xa3, 0x8e,
        0x08, 0x8a, 0x22, 0xb6, 0x5e, 0x52, 0xbc, 0x51, 0x4d, 0x16, 0xcc, 0xf8, 0x06, 0x81, 0x8c,
        0xe9, 0x1a, 0xb7, 0x79, 0x37, 0x36, 0x5a, 0xf9, 0x0b, 0xbf, 0x74, 0xa3, 0x5b, 0xe6, 0xb4,
        0x0b, 0x8e, 0xed, 0xf2, 0x78, 0x5e, 0x42, 0x87, 0x4d,
    ];
    let plaintext = b"Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it.";

    assert_eq!(ciphertext.len(), plaintext.len());

    let mut group = c.benchmark_group("ChaCha stream");

    let transcript = merlin::Transcript::new(b"chacha20");
    let pc_gens = PedersenGens::default();
    let bp_gens = {
        // Compute maximum amount of multipliers from an example transcript.
        let mut transcript = transcript.clone();
        let mut prover = Prover::new(&pc_gens, &mut transcript);

        ChaCha20Stream::prove_preimage(
            &mut prover,
            Some(RFC_7539_KEY),
            RFC_7539_NONCE,
            &ciphertext,
            Some(plaintext),
        )
        .unwrap();

        BulletproofGens::new(prover.metrics().multipliers.next_power_of_two(), 1)
    };

    for len in 0..ciphertext.len() {
        let ciphertext = &ciphertext[0..len];
        let plaintext = &plaintext[0..len];
        group.throughput(Throughput::Bytes(len as u64));

        group.bench_with_input(
            BenchmarkId::new("chacha-stream-prove", len),
            &(ciphertext, plaintext),
            |b, (ciphertext, plaintext)| {
                b.iter(|| {
                    let mut transcript = transcript.clone();
                    let mut prover = Prover::new(&pc_gens, &mut transcript);
                    ChaCha20Stream::prove_preimage(
                        &mut prover,
                        Some(RFC_7539_KEY),
                        RFC_7539_NONCE,
                        &ciphertext,
                        Some(plaintext),
                    )
                    .unwrap();
                    prover.prove(&bp_gens).unwrap()
                });
            },
        );

        group.bench_with_input(
            BenchmarkId::new("chacha-stream-verify", len),
            &ciphertext,
            |b, ciphertext| {
                let proof = {
                    let mut transcript = transcript.clone();
                    let mut prover = Prover::new(&pc_gens, &mut transcript);
                    ChaCha20Stream::prove_preimage(
                        &mut prover,
                        Some(RFC_7539_KEY),
                        RFC_7539_NONCE,
                        &ciphertext,
                        Some(plaintext),
                    )
                    .unwrap();
                    prover.prove(&bp_gens).unwrap()
                };

                b.iter(|| {
                    let mut transcript = transcript.clone();
                    let mut verifier = Verifier::new(&mut transcript);
                    ChaCha20Stream::prove_preimage(
                        &mut verifier,
                        None,
                        RFC_7539_NONCE,
                        &ciphertext,
                        None,
                    )
                    .unwrap();
                    verifier.verify(&proof, &pc_gens, &bp_gens).unwrap()
                });
            },
        );
    }

    group.finish();
}

criterion_group! {
    name = chacha;
    config = Criterion::default().sample_size(10);
    targets =
        chacha_apply_stream,
}

criterion_main!(chacha);
